@echo off
rem echo "Starting";
rem Set-ExecutionPolicy Unrestricted -Force
cmd /k "%~dp0venv\Scripts\activate.bat & cd /d %~dp0 & python main.py & exit"
rem echo "Done";
