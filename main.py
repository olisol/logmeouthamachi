import time

import win32api
import win32serviceutil
import subprocess
import win32ui
import win32gui
import win32con
from PIL import Image
import numpy as np
import wmi


def window_exists():
    try:
        hwnd = win32gui.FindWindow(None, "LogMeIn Hamachi")
        wDC = win32gui.GetWindowDC(hwnd)
        dcObj = win32ui.CreateDCFromHandle(wDC)
        cDC = dcObj.CreateCompatibleDC()
        dataBitMap = win32ui.CreateBitmap()
        w, h = 20, 20
        dataBitMap.CreateCompatibleBitmap(dcObj, w, h)
        cDC.SelectObject(dataBitMap)
        cDC.BitBlt((0, 0), (w, h), dcObj, (0, 0), win32con.SRCCOPY)
        dataBitMap.SaveBitmapFile(cDC, "img.bmp")
        # Free Resources
        dcObj.DeleteDC()
        cDC.DeleteDC()
        win32gui.ReleaseDC(hwnd, wDC)
        win32gui.DeleteObject(dataBitMap.GetHandle())

        im = np.asarray(Image.open('img.bmp'))
        return np.any(im != 0)

    except win32ui.error as e:
        return False


serviceName = "Hamachi2Svc"
win32serviceutil.StartService(serviceName)

p = subprocess.Popen(r"C:\Program Files (x86)\LogMeIn Hamachi\hamachi-2-ui.exe")
while window_exists():
    time.sleep(20)
p.terminate()

win32serviceutil.StopService(serviceName)
wmi_instance = wmi.WMI()
for process in wmi_instance.Win32_Process():
    if process.Name == "LMIGuardianSvc.exe":
        pid = process.ProcessId
        PROCESS_TERMINATE = 1
        handle = win32api.OpenProcess(PROCESS_TERMINATE, False, pid)
        win32api.TerminateProcess(handle, -1)
        win32api.CloseHandle(handle)